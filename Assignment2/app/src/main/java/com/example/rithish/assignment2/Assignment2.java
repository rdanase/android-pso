package com.example.rithish.assignment2;

/**
 * Created by Rithish on 9/28/2015.
 */
public class Assignment2
{
    private static double getAvg(int [] data)
    {
        int sum = 0;
        double average;
        int count = data.length;
        for (int i = 0; i < data.length; i++)
        {
            sum = sum + data[i];
        }
        average = (double)sum/count;
        System.out.println("Average:" +average);
        return average;
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        System.out.println("Average of Integers");
        int [] d = {1,2,3,4,5};
        getAvg(d);
    }
}
